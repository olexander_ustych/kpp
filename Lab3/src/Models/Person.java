package Models;

import Attributes.Position;

import java.util.List;

public class Person {
    public String FirstName;
    public String LastName;
    public Attributes.Position Position;
    public int Year;
    public int Salary;

    @Override
    public String toString() {
        return "FirstName: " + this.FirstName + ", LastName:" +
                LastName + ", Position: " + Position +
                ", Year: " + Year + ", Salary: " + Salary;
    }
}
