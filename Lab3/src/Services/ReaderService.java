package Services;

import Attributes.Position;
import Models.Person;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReaderService {
    public static List<Person> ReadFromFile(String fileName) {
        List<Person> Persons = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] PersonData = line.split(" ");
                Person person = new Person();
                person.FirstName = PersonData[0];
                person.LastName = PersonData[1];
                for (var position: Position.values()) {
                    if(PersonData[2].compareTo(position.toString()) == 0) {
                        person.Position = position;
                        break;
                    }
                }
                person.Year = Integer.parseInt(PersonData[3]);
                person.Salary = Integer.parseInt(PersonData[4]);
                Persons.add(person);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Persons;
    }
}
