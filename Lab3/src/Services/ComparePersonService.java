package Services;

import Models.Person;

import java.util.Comparator;

public class ComparePersonService {
    public static class AgeSorting implements Comparator<Person> {
        @Override
        public int compare(Person p1, Person p2) {
            Integer firstAge = p1.Year;
            Integer secondAge = p2.Year;
            return firstAge.compareTo(secondAge);
        }
    }

    public static class SalarySoring implements Comparator<Person> {
        @Override
        public int compare(Person p1, Person p2) {
            Integer firstSalary = p1.Salary;
            Integer secondSalary = p2.Salary;
            return firstSalary.compareTo(secondSalary);
        }
    }
}
