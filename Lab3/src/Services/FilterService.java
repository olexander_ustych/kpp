package Services;

import Attributes.Position;
import Models.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterService {
    public static HashMap<Position, List<Person>> getPositionEmployee(List<Person> Persons) {
        HashMap<Position, List<Person>> PositionEmployee = new HashMap<>();
        for (var person: Persons) {
            if(PositionEmployee.containsKey(person.Position))
            {
                var ListOfEmployeesOfPosition = PositionEmployee.get(person.Position);
                ListOfEmployeesOfPosition.add(person);
                PositionEmployee.put(person.Position, ListOfEmployeesOfPosition);
            }
            else
            {
                var ListOfEmployeesOfPosition = new ArrayList<Person>();
                ListOfEmployeesOfPosition.add(person);
                PositionEmployee.put(person.Position, ListOfEmployeesOfPosition);
            }
        }
        return  PositionEmployee;
    }
}
