import Attributes.Position;
import Attributes.SalaryRang;
import Models.Person;
import Services.ComparePersonService;
import Services.FilterService;
import Services.ReaderService;
import com.sun.source.tree.Tree;

import java.io.Console;
import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private final static String fileName1 = "C:\\Users\\FireM\\Java IDEA\\KPP_Java\\Lab3\\src\\Txt_Files\\Employees1.txt";
            //"./Txt_Files/Employees1.txt";
    private final static String fileName2 = "C:\\Users\\FireM\\Java IDEA\\KPP_Java\\Lab3\\src\\Txt_Files\\Employees2.txt";
    public static void main(String[] args) {
            Integer choice = null;
            do {
                Scanner newScChoicer = new Scanner(System.in);
                choice = null;
                while(choice == null) {
                    try {
                        System.out.println("\n\n\n0 - Exit" +
                                "\nTask 1:\n" +
                                "1 - List of all employees, " +
                                "2 - Map of all employees, 3 - Map of all 'Producers', " +
                                "4 - Map of all 'Producers' with full names, " +
                                "5 - Min and max age of all positions\n" +
                                "Task 2:\n" +
                                "6 -  Max and min salary, " +
                                "7 - All Salary rang\n" +
                                "Task 3:\n" +
                                "8 - Lastnames of employees");
                        System.out.println("\nEnter your choose: ");
                        int nextChoice = newScChoicer.nextInt();
                        choice = nextChoice;
                    }
                    catch (InputMismatchException e) {
                        System.out.println("You entered wrong value! Try again!");
                        newScChoicer.nextLine();
                    }
                }
                newScChoicer.nextLine();


                var Persons = ReaderService.ReadFromFile(fileName1);
                var PersonPosition = FilterService.getPositionEmployee(Persons);
                switch (choice) {
                    case 0: {
                        System.out.println("Bye!");
                        break;
                    }

                    // 1
                    case 1: {
                        // List of all employees
                        System.out.println("List of all employees: ");
                        for (var person: Persons) {
                            System.out.println(person.toString());
                        }
                        System.out.println("");

                        break;
                    }
                    case 2: {
                        // Map of all employees
                        System.out.println("Map of all employees: ");
                        for (var position: PersonPosition.keySet() ) {
                            System.out.println(position.toString() + ":");
                            for (var person: PersonPosition.get(position)) {
                                System.out.println(person.toString());
                            }
                            System.out.println("");
                        }

                        break;
                    }
                    case 3: {
                        // Map of all 'Producers'
                        System.out.println("Map of all 'Producers': ");
                        for (var person: PersonPosition.get(Position.Producer)) {
                            System.out.println(person.toString());
                        }
                        System.out.println();

                        break;
                    }
                    case 4: {
                        // Map of all 'Producers' with full names
                        System.out.println("Map of all 'Producers' with full names: ");
                        List<String> PersonsProducersFullName = new ArrayList<>();
                        for (var person: PersonPosition.get(Position.Producer)) {
                            PersonsProducersFullName.add(person.FirstName + " " + person.LastName);
                        }
                        HashMap<Position, List<String>> PositionFullName = new HashMap<>();
                        PositionFullName.put(Position.Producer, PersonsProducersFullName);
                        for (var person: PositionFullName.get(Position.Producer)) {
                            System.out.println(person);
                        }
                        System.out.println();

                        break;
                    }
                    case 5: {
                        // Min and max age of all positions
                        for (var position: Position.values()){
                            PersonPosition.get(position).sort(new ComparePersonService.AgeSorting());
                            System.out.println("Min and max age of " + position.toString() + ": ");
                            var TheYoungest = PersonPosition.get(position).get(0);
                            var TheOldest = PersonPosition.get(position)
                                    .get(PersonPosition.get(position).toArray().length - 1);
                            System.out.println(TheYoungest.toString());
                            System.out.println(TheOldest.toString());
                            System.out.println();
                        }

                        break;
                    }

                    //2
                    case 6: {
                        // Max and min salary
                        List<Person> minSalary = new ArrayList<>();
                        List<Person> maxSalary = new ArrayList<>();
                        for (var position: Position.values()) {
                            PersonPosition.get(position).sort(new ComparePersonService.SalarySoring());
                            minSalary.add(PersonPosition.get(position).get(0));
                            maxSalary.add(PersonPosition.get(position).get(PersonPosition.get(position).toArray().length - 1));
                        }
                        minSalary.sort(new ComparePersonService.SalarySoring());
                        maxSalary.sort(new ComparePersonService.SalarySoring());
                        System.out.println("Max and min salary: ");
                        System.out.println(minSalary.get(0));
                        System.out.println(maxSalary.get(maxSalary.toArray().length - 1));
                        System.out.println();

                        break;
                    }
                    case 7: {
                        // All Salary rang
                        TreeMap<SalaryRang, List<Person>> SalaryPerson = new TreeMap<>();
                        for (var position: Position.values()) {
                            for (var person: PersonPosition.get(position)) {
                                SalaryRang salaryRang;
                                if(person.Salary < 20000) {
                                    salaryRang = SalaryRang.Low;
                                } else if (person.Salary < 40000) {
                                    salaryRang = SalaryRang.Mid;
                                } else {
                                    salaryRang = SalaryRang.High;
                                }

                                if(SalaryPerson.containsKey(salaryRang)) {
                                    var persons = SalaryPerson.get(salaryRang);
                                    persons.add(person);
                                    SalaryPerson.put(salaryRang, persons);
                                } else {
                                    var persons = new ArrayList<Person>();
                                    persons.add(person);
                                    SalaryPerson.put(salaryRang, persons);
                                }
                            }
                        }
                        for (var salaryRang: SalaryRang.values()) {
                            System.out.println("Salary rang " + salaryRang.toString() + ": ");
                            for (var person: SalaryPerson.get(salaryRang)) {
                                System.out.println(person);
                            }
                            System.out.println();
                        }

                        break;
                    }

                    //3
                    case 8: {
                        // Lastnames of employees
                        var Persons2 = ReaderService.ReadFromFile(fileName2);
                        var AllPersons = Stream.concat(Persons.stream(),
                                Persons2.stream()).collect(Collectors.toList());
                        HashSet<String> PersonsLastNames = new HashSet<>(AllPersons.stream()
                                .map(person -> person.LastName)
                                .collect(Collectors.toList()));
                        for (var LastNames: PersonsLastNames ) {
                            System.out.println(LastNames);
                        }

                        Scanner newSc = newScChoicer;//new Scanner(System.in);
                        Integer minAge = null;
                        while(minAge == null) {
                            try {
                                System.out.println("\nEnter min age: ");
                                int nextMinAge = newSc.nextInt();
                                minAge = nextMinAge;
                            }
                            catch (InputMismatchException e) {
                                System.out.println("You entered wrong value! Try again!");
                                newSc.nextLine();
                            }
                        }
                        //newSc.close();
                        Integer finalMinAge = minAge;
                        AllPersons = AllPersons.stream()
                                .filter(person -> Year.now().getValue() - person.Year > finalMinAge)
                                .collect(Collectors.toList());
                        PersonsLastNames = new HashSet<>(AllPersons.stream()
                                .map(person -> person.LastName)
                                .collect(Collectors.toList()));
                        System.out.println("Lastnames of employees older than " + minAge);
                        for (var LastNames: PersonsLastNames ) {
                            System.out.println(LastNames);
                        }

                        break;
                    }
                    default: {
                        System.out.println("Wrong input!");
                        break;
                    }
            }
        } while(choice != 0);
    }

    public static void DeleteMethod(List<Person> Persons) {
        for (var person: Persons) {
            if(person.LastName.compareTo("Мізін") == 0) {
                Persons.remove(person);
            }
        }
        Persons.removeIf(person -> person.LastName.compareTo("Мізін") == 0);
    }
}


        /*
        //1
        var Persons = ReaderService.ReadFromFile(fileName1);
        System.out.println("List of all employees: ");
        for (var person: Persons) {
            System.out.println(person.toString());
        }
        System.out.println("");
        var PersonPosition = FilterService.getPositionEmployee(Persons);
        System.out.println("Map of all employees: ");
        for (var position: PersonPosition.keySet() ) {
            System.out.println(position.toString() + ":");
            for (var person: PersonPosition.get(position)) {
                System.out.println(person.toString());
            }
            System.out.println("");
        }


        System.out.println("Map of all 'Producers': ");
        for (var person: PersonPosition.get(Position.Producer)) {
            System.out.println(person.toString());
        }
        System.out.println();

        System.out.println("Map of all 'Producers' with full names: ");
        List<String> PersonsProducersFullName = new ArrayList<>();
        for (var person: PersonPosition.get(Position.Producer)) {
            PersonsProducersFullName.add(person.FirstName + " " + person.LastName);
        }
        HashMap<Position, List<String>> PositionFullName = new HashMap<>();
        PositionFullName.put(Position.Producer, PersonsProducersFullName);
        for (var person: PositionFullName.get(Position.Producer)) {
            System.out.println(person);
        }
        System.out.println();

        for (var position: Position.values()){
            PersonPosition.get(position).sort(new ComparePersonService.AgeSorting());
            System.out.println("Min and max age of " + position.toString() + ": ");
            var TheYoungest = PersonPosition.get(position).get(0);
            var TheOldest = PersonPosition.get(position)
                    .get(PersonPosition.get(position).toArray().length - 1);
            System.out.println(TheYoungest.toString());
            System.out.println(TheOldest.toString());
            System.out.println();
        }



        //2
        List<Person> minSalary = new ArrayList<>();
        List<Person> maxSalary = new ArrayList<>();
        for (var position: Position.values()) {
            PersonPosition.get(position).sort(new ComparePersonService.SalarySoring());
            minSalary.add(PersonPosition.get(position).get(0));
            maxSalary.add(PersonPosition.get(position).get(PersonPosition.get(position).toArray().length - 1));
        }
        minSalary.sort(new ComparePersonService.SalarySoring());
        maxSalary.sort(new ComparePersonService.SalarySoring());
        System.out.println("\n\n\nMax and min salary: ");
        System.out.println(minSalary.get(0));
        System.out.println(maxSalary.get(maxSalary.toArray().length - 1));
        System.out.println();

        TreeMap<SalaryRang, List<Person>> SalaryPerson = new TreeMap<>();
        for (var position: Position.values()) {
            for (var person: PersonPosition.get(position)) {
                SalaryRang salaryRang;
                if(person.Salary < 20000) {
                    salaryRang = SalaryRang.Low;
                } else if (person.Salary < 40000) {
                    salaryRang = SalaryRang.Mid;
                } else {
                    salaryRang = SalaryRang.High;
                }

                if(SalaryPerson.containsKey(salaryRang)) {
                    var persons = SalaryPerson.get(salaryRang);
                    persons.add(person);
                    SalaryPerson.put(salaryRang, persons);
                } else {
                    var persons = new ArrayList<Person>();
                    persons.add(person);
                    SalaryPerson.put(salaryRang, persons);
                }
            }
        }
        for (var salaryRang: SalaryRang.values()) {
            System.out.println("Salary rang " + salaryRang.toString() + ": ");
            for (var person: SalaryPerson.get(salaryRang)) {
                System.out.println(person);
            }
            System.out.println();
        }
        System.out.println("\n\n");


        //3
        var Persons2 = ReaderService.ReadFromFile(fileName2);
        var AllPersons = Stream.concat(Persons.stream(),
                Persons2.stream()).collect(Collectors.toList());
        HashSet<String> PersonsLastNames = new HashSet<>(AllPersons.stream()
                .map(person -> person.LastName)
                .collect(Collectors.toList()));
        for (var LastNames: PersonsLastNames ) {
            System.out.println(LastNames);
        }

        Scanner newSc = new Scanner(System.in);
        Integer minAge = null;
        while(minAge == null) {
            try {
                System.out.println("\nEnter min age: ");
                 int nextMinAge = newSc.nextInt();
                 minAge = nextMinAge;
            }
            catch (InputMismatchException e) {
                System.out.println("You entered wrong value! Try again!");
                newSc.nextLine();
            }
        }
        newSc.close();
        Integer finalMinAge = minAge;
        AllPersons = AllPersons.stream()
                .filter(person -> Year.now().getValue() - person.Year > finalMinAge)
                .collect(Collectors.toList());
        PersonsLastNames = new HashSet<>(AllPersons.stream()
                .map(person -> person.LastName)
                .collect(Collectors.toList()));
        System.out.println("Lastnames of employees older than " + minAge);
        for (var LastNames: PersonsLastNames ) {
            System.out.println(LastNames);
        }
        */

        /*
        TreeSet<Integer> sortedNumbers = new TreeSet<>();
        sortedNumbers.add(5);
        sortedNumbers.add(2);
        sortedNumbers.add(8);
        sortedNumbers.add(1);
        sortedNumbers.add(8);
        sortedNumbers.add(5);
        for (var number: sortedNumbers ) {
            System.out.println(number);
        }
        1
        2
        5
        8
        */

        /*System.out.println("Min and max age of producers: ");
        var TheYoungestProducer = PersonPosition.get(Position.Producer.toString()).get(0);
        var TheOldestProducer = PersonPosition.get(Position.Producer.toString())
                .get(PersonPosition.get(Position.Producer.toString()).toArray().length - 1);
        System.out.println();
        System.out.println(TheYoungestProducer.toString());
        System.out.println(TheOldestProducer.toString());

        System.out.println("Min and max age of operators: ");
        PersonPosition.get(Position.Operator).sort(new ComparePersonService.AgeSorting());
        var TheYoungestOperator = PersonPosition.get(Position.Operator.toString()).get(0);
        var TheOldestOperator = PersonPosition.get(Position.Operator.toString())
                .get(PersonPosition.get(Position.Operator.toString()).toArray().length - 1);
        System.out.println();
        System.out.println(TheYoungestOperator.toString());
        System.out.println(TheOldestOperator.toString());

        System.out.println("Min and max age of directors: ");
        var TheYoungestDirector = PersonPosition.get(Position.Director.toString()).get(0);
        var TheOldestDirector = PersonPosition.get(Position.Director.toString())
                .get(PersonPosition.get(Position.Director.toString()).toArray().length - 1);
        System.out.println();
        System.out.println(TheYoungestDirector.toString());
        System.out.println(TheOldestDirector.toString());*/



        /*System.out.println("");
        System.out.println("");
        System.out.println("");

        TreeMap<String, Integer> sortedAgeMap = new TreeMap<>();
        sortedAgeMap.put("Alice", 25);
        sortedAgeMap.put("Bob", 30);
        sortedAgeMap.put("Alice", 25);
        sortedAgeMap.put("Alice", 26);
        sortedAgeMap.put("Ivan", 27);

        for (var map: sortedAgeMap.keySet()) {
            System.out.println("Name: " + map + ", Age: " + sortedAgeMap.get(map));
        }*/
        /*
        Name: Alice, Age: 26
        Name: Bob, Age: 30
        Name: Ivan, Age: 27
        */