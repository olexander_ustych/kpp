package com.example.lab7;

import Models.Bank;
import Models.Client;
import ViewModel.BankViewModel;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class BankSimulationController {
    /*@FXML
    private Label welcomeText;
    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }*/
    @FXML
    private VBox vbox; // Reference to the VBox in the FXML file



    @FXML
    private TextField NumberTextField;
    @FXML
    private Label NumberLabel;
    @FXML
    private HBox MainHBox;
    @FXML
    private HBox ThreadInfo;
    @FXML
    private Button Start;

    private int ThreadsCount;
    private List<Client> clients = new LinkedList<>();
    private List<Thread> threads = new LinkedList<>();
    private List<Label> threadsInfo = new LinkedList<>();

    @FXML
    protected void onStartClick() {
        try{
            ThreadsCount = Integer.parseInt(NumberTextField.getText());
            //NumberLabel.setText(String.valueOf(ThreadsCount));
            this.Start.setDisable(true);
        }
        catch (NumberFormatException ex) {
            NumberLabel.setText(ex.getMessage());
        }

        //Bank bank = new Bank(10000);
        BankViewModel bankViewModel = new BankViewModel(new Bank(10000));

        for (int i = 0; i < ThreadsCount; i++) {

            Label threadsText = new Label();
            threadsText.setWrapText(true);
            threadsText.setPadding(new Insets(20.0));
            threadsText.maxHeight(300);
            MainHBox.getChildren().add(threadsText);

            Label threadInfo = new Label();
            threadInfo.setWrapText(true);
            threadInfo.setPadding(new Insets(20));
            threadInfo.maxHeight(150);
            ThreadInfo.getChildren().add(threadInfo);

            var client = new Client(
                    bankViewModel, threadsText, threadInfo
            );
            var thread = new Thread(client);
            try{
                thread.setPriority(new Random().nextInt(1, 11));
            }
            catch (Exception ex) {
                //InvocationTargetException
                thread = new Thread(client);
                thread.setPriority(5);
            }

            clients.add(client);
            threads.add(thread);
            threadsInfo.add(threadInfo);

            thread.start();
            Suspended.add(false);
        }
    }

    private List<Boolean> Suspended = new LinkedList<>();

    @FXML
    private Button Suspend;
    @FXML
    protected void onSuspendClick() {
        Suspend.setDisable(true);
        for(var thread: threads) {
            if(Suspended.get(threads.indexOf(thread)) == false){
                thread.suspend();
                Suspended.set(threads.indexOf(thread), true);
            } else {
                thread.resume();
                Suspended.set(threads.indexOf(thread), false);
            }

            var infos = threadsInfo.get(threads.indexOf(thread)).getText().split("\n");

            infos[infos.length - 1] = "Time last change: " +
                    LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            var resInfo = "";
            for (var info: infos) {
                if(Arrays.stream(infos).toList().indexOf(info) != infos.length - 1) {
                    resInfo += info + "\n";
                } else {
                    resInfo += info;
                }
            }

            threadsInfo.get(threads.indexOf(thread)).setText(resInfo);
        }
        Suspend.setDisable(false);
    }

    @FXML
    private Button SuspendByWait;
    @FXML
    private Button ResumeByWait;
    private Object lock = new Object();
    @FXML
    protected void onSuspendByWaitClick() {
        SuspendByWait.setDisable(true);
        ResumeByWait.setDisable(false);
        synchronized (lock) {
            for(var thread: threads) {
                if(Suspended.get(threads.indexOf(thread)) == false) {
                    try {
                        lock.wait();
                    }
                    catch (InterruptedException ex) {
                        lock.notify();
                    }

                    Suspended.set(threads.indexOf(thread), true);
                }
            }
        }
    }
    @FXML
    protected void onResumeByWaitClick() {
        SuspendByWait.setDisable(false);
        ResumeByWait.setDisable(true);
        synchronized (lock) {
            for(var thread: threads) {
                if(Suspended.get(threads.indexOf(thread)) == true) {
                    lock.notify();

                    Suspended.set(threads.indexOf(thread), true);
                }
            }
        }
    }
}