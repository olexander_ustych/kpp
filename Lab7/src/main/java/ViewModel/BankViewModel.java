package ViewModel;

import Models.Bank;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.security.InvalidParameterException;
import java.util.concurrent.locks.ReentrantLock;

public class BankViewModel {
    private Bank bank;
    private Object lock;
    private ReentrantLock Rlock;
    public BankViewModel(Bank bank)
    {
        this.bank = bank;
        this.lock = new Object();
        this.Rlock = new ReentrantLock();
    }

    public void withdrawCash(int amount, Label textField) {
        if(amount < 0) {
            throw new InvalidParameterException("The parameter 'amount' can't be less that zero!");
        }

        //synchronized (lock) {
        Rlock.lock();
        try{
            final var cash = bank.getTotalCash();
            Platform.runLater(() -> {
                textField.setText("Cash in bank before withdraw: " + cash + "\n");
                textField.setText(textField.getText() + "Amount: " + amount + "\n");
            });
            bank.withdrawCash(amount);
            final var nextCash = bank.getTotalCash();
            Platform.runLater(() -> {
                textField.setText(textField.getText() + "Cash in bank after withdraw: " + nextCash + "\n");
            });
        }
        finally {
            Rlock.unlock();
        }

        //}
    }

    public void depositCash(int amount, Label textField) {
        if(amount < 0) {
            throw new InvalidParameterException("The parameter 'amount' can't be less that zero!");
        }

        //synchronized (lock) {
        Rlock.lock();
        try{
            final var cash = bank.getTotalCash();
            Platform.runLater(() -> {
                textField.setText("Cash in bank before deposit: " + cash + "\n");
                textField.setText(textField.getText() + "Amount: " + amount + "\n");
            });
            bank.depositCash(amount);
            final var  nextCash = bank.getTotalCash();
            Platform.runLater(() -> {
                textField.setText(textField.getText() + "Cash in bank after depositCash: " + nextCash + "\n");
            });
        }
        finally {
            Rlock.unlock();
        }

        //}
    }
}
