package Models;

import ViewModel.BankViewModel;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Client implements Runnable {
    private final BankViewModel bank;
    private final Label textField;
    private final Label info;
    public Client(BankViewModel bank, Label textField, Label info)
    {
        this.bank = bank;
        this.textField = textField;
        this.info = info;
    }

    public void withdrawCash(int amount)
    {
        bank.withdrawCash(amount, textField);
    }

    public void depositCash(int amount)
    {
        bank.depositCash(amount, textField);
    }

    @Override
    public void run() {

        var thread = Thread.currentThread();
        //thread.setPriority(new Random().nextInt(5));
        var pr = thread.getPriority();
        var threadName = thread.getName();
        Platform.runLater(() -> {
            info.setText(threadName + "\n" + pr + "\n" + "Time last change: " +
                    LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        });

        Random random = new Random();
        int i = random.nextInt(1, 3);
        while (true) {
            if(i%2 == 0) {
                withdrawCash(random.nextInt(100));
            }
            if(i%2 == 1) {
                depositCash(random.nextInt(100));
            }

            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

            if(i > 25) {
                break;
            }
            i++;
        }
    }
}
