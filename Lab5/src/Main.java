import Services.RegularExpressionService;

public class Main {
    public static void main(String[] args) {
        String input = "abc123xyz456";
        String regex = "[0-9]+";
        //RegularExpressionService.Match(input, regex);

        //System.out.println("\n\n\n");

        String[] wordsToDelete = new String[]
                {"\\ba\\b", "\\bthe\\b", "\\bor\\b", "\\bare\\b", "\\bon\\b", "\\bin\\b", "\\bout\\b"};
        String sentence = "We are heroes on the top roof or " +
                "a building which are in the city which is out of this world.";
        var res = RegularExpressionService.DeleteAllWords(sentence, wordsToDelete);
        System.out.println(res);
    }
}