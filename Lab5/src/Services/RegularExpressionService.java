package Services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionService {
    public static void Match(String input, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println("Match found: " + matcher.group());
        }
    }

    public static String DeleteAllWords(String sentence, String[] words) {
        for(var word: words) {
            Pattern pattern = Pattern.compile(word);
            Matcher matcher = pattern.matcher(sentence);

            while (matcher.find()) {
                System.out.println("Match found: " + matcher.group());
                int start = matcher.start();
                int end = matcher.end();
                System.out.println("Find the word '" + sentence.substring(start,end) +
                        "' from "+ start + " to " + (end-1) + " position");
                System.out.println();
            }
            sentence = matcher.replaceAll("");
        }
        return sentence;
    }
}
