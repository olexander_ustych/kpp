package Services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegularExpressionServiceTest {
    @Test
    void DeleteAllWords() {
        String[] wordsToDelete = new String[]
                {"\\ba\\b", "\\bthe\\b", "\\bor\\b", "\\bare\\b", "\\bon\\b", "\\bin\\b", "\\bout\\b"};
        String sentence = "We are heroes on the top roof or " +
                "a building which are in the city which is out of this world.";
        String expected = "We  heroes   top roof  " +
                " building which    city which is  of this world.";
        var res = RegularExpressionService.DeleteAllWords(sentence, wordsToDelete);
        System.out.println(res);
        assertEquals(expected, res);
    }

    @Test
    void DeleteAllWordsIfSentenceIsEmpty() {
        String[] wordsToDelete = new String[]
                {"\\ba\\b", "\\bthe\\b", "\\bor\\b", "\\bare\\b", "\\bon\\b", "\\bin\\b", "\\bout\\b"};
        String sentence = "";
        String expected = "";
        var res = RegularExpressionService.DeleteAllWords(sentence, wordsToDelete);
        System.out.println(res);
        assertEquals(expected, res);
    }

    @Test
    void DeleteAllWordsIfSentenceHasNoNeededWords() {
        String[] wordsToDelete = new String[]
                {"\\ba\\b", "\\bthe\\b", "\\bor\\b", "\\bare\\b", "\\bon\\b", "\\bin\\b", "\\bout\\b"};
        String sentence = "We heroes top roof " +
            "building which city which is of this world.";
        String expected = "We heroes top roof " +
                "building which city which is of this world.";
        var res = RegularExpressionService.DeleteAllWords(sentence, wordsToDelete);
        System.out.println(res);
        assertEquals(expected, res);
    }
}