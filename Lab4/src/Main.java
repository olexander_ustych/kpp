import Models.Person;
import Services.*;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        List<Person> persons = null;
        Integer choose = null;

        Scanner newSc = new Scanner(System.in);
        do {
            newSc = new Scanner(System.in);
            System.out.println("0 - mock, 1 - enter data");
            try {
                choose = newSc.nextInt();
            }
            catch (Exception e) {
                System.out.println("Invalid data entered");
                choose = -1;
                newSc.nextLine();
            }
        } while (choose != 1 && choose != 0);
        newSc.nextLine();



        if(choose == 0)
        {
            persons = MockService.GetFriends();
            PrintService.PrintPersons(persons);
        }
        else
        {
            persons = ScannerService.EnterData();
            PrintService.PrintPersons(persons);
        }

        var Meetings = BestTimeFinderService.FindBestTime(persons);
        PrintService.PrintDaysMeeting(Meetings);

        var bestTimeDuration = BesTimeComputeService.ComputeBestTime(Meetings);
        System.out.println("Duration: ");
        PrintService.PrintDuration(bestTimeDuration);



        newSc.nextLine();


        // приклади
        /*
        05/05/2023 11:11
        05/05/2023 19:15

        05/05/2023 12:11
        05/05/2023 20:16
        */





        // старий якийись код
        /*var bestTime = BesTimeComputeService.ComputeBestTime(Meetings);
        var format = TimeFormatterService.getDateTimeFormatter();
        for(var time: bestTime.keySet()) {
            System.out.println(time + ": " + bestTime.get(time).format(format));
        }*/






        // Чекав роботу ZonedDateTime
        /*System.out.println("\n\n\n");

        ZoneOffset offset1 = ZoneOffset.ofHours(2); // UTC+02:00
        ZoneOffset offset2 = ZoneOffset.ofHours(-5); // UTC-05:00

        // Calculate the difference between offset1 and offset2
        Duration difference = Duration.ofSeconds(offset1.getTotalSeconds() - offset2.getTotalSeconds());

        long hours = difference.toHours(); // Get the number of hours
        long minutes = difference.toMinutesPart(); // Get the number of minutes (excluding hours)

        System.out.println("ZoneOffset 1: " + offset1);
        System.out.println("ZoneOffset 2: " + offset2);
        System.out.println("Offset Difference: " + hours + " hours, " + minutes + " minutes");

        LocalDateTime d1 = LocalDateTime.ofEpochSecond(
                LocalDateTime.of(
                        2023, 10, 10, 22, 15, 15).toEpochSecond(ZoneOffset.UTC),
                0,
                ZoneOffset.ofHours(1)
        );

        var formatter = TimeFormatterService.getDateTimeFormatter();
        String FormattedDateTime = d1.format(formatter);
        System.out.println(FormattedDateTime);


        var offsetOfDate = d1.atOffset(ZoneOffset.ofHours(2));
        var f1 = offsetOfDate.format(formatter);
        System.out.println(f1);


        ZonedDateTime z1 = ZonedDateTime.of(2023, 10, 10, 0,0,0,0, ZoneOffset.ofHours(1));
        System.out.println(z1.getOffset());
        var zone = z1.getOffset();
        Integer diff =  null;
        diff = zone.get(ChronoField.OFFSET_SECONDS);
        System.out.println(diff);*/
    }
}




/*TreeMap<String, Integer> sortedAgeMap = new TreeMap<>();
sortedAgeMap.put("Bob", 30);
sortedAgeMap.put("Alice", 25);
sortedAgeMap.put("Darvin", 11);
sortedAgeMap.put("Alice", 26);
sortedAgeMap.put("Charles", 21);

for (var key: sortedAgeMap.keySet()) {
    System.out.println("Key: "+ key + " Value: " + sortedAgeMap.get(key));
}

System.out.println();

HashMap<String, Integer> ageMap = new HashMap<>();
ageMap.put("Bob", 30);
ageMap.put("Darvin", 11);
ageMap.put("Alice", 25);
ageMap.put("Alice", 26);
ageMap.put("Charles", 21);

for (var key: ageMap.keySet()) {
    System.out.println("Key: "+ key + " Value: " + ageMap.get(key));
}*/
/* res
Key: Alice Value: 26
Key: Bob Value: 30
Key: Charles Value: 21
Key: Darvin Value: 11

Key: Darvin Value: 11
Key: Charles Value: 21
Key: Bob Value: 30
Key: Alice Value: 26
*/