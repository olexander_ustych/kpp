package Services;

import Models.Person;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;

public class MockService {
    public static List<Person> GetFriends() {
        String[] names = {"John", "Ivan", "Sasha", "Vasyl", "Jorge", "Andriy", "Taras"};
        List<Person> Persons = new LinkedList<>();

        names = new String[]{"John", "Ivan", "Sasha"};

        for (var name: names) {
            Person person = new Person();
            person.Name = name;

            person.Schedule = new ArrayList<>();

            for(int i = 0; i < 5; i++)
            {
                /*
                    Краще юзати ZonedDateTime, приклад є в мене в мейні,
                    за подробицями до Максима Фаєвського
                */
                LocalDateTime startDateTime = LocalDateTime.of(2023, 10, 23, 0,0);
                LocalDateTime endDateTime = LocalDateTime.of(2023, 10, 29, 23, 59);

                Random random = new Random();
                long minEpochSeconds = startDateTime.toEpochSecond(ZoneOffset.UTC);
                long maxEpochSeconds = endDateTime.toEpochSecond(ZoneOffset.UTC);
                long randomEpochSeconds = minEpochSeconds + (long)(Math.random() * (maxEpochSeconds - minEpochSeconds + 1));

                LocalDateTime firstDateTime = LocalDateTime.ofEpochSecond(randomEpochSeconds, 0, ZoneOffset.UTC);

                minEpochSeconds = firstDateTime.toEpochSecond(ZoneOffset.UTC);
                randomEpochSeconds = minEpochSeconds + (long)(Math.random() * (maxEpochSeconds - minEpochSeconds + 1));
                LocalDateTime secondDateTime = LocalDateTime.ofEpochSecond(randomEpochSeconds, 0, ZoneOffset.UTC);

                AbstractMap.SimpleImmutableEntry<LocalDateTime, LocalDateTime> DateTimes =
                        new AbstractMap.SimpleImmutableEntry<>(firstDateTime, secondDateTime);

                person.Schedule.add(DateTimes);
            }

            Collections.sort(person.Schedule,
                    (sch1, sch2) -> sch1.getKey().compareTo(sch2.getKey()));




            // старий код з днями тижня
            /*for (var day: DayOfWeek.values()) {
                LocalDateTime startDateTime = LocalDateTime.of(2023, 10, 23, 0,0);
                LocalDateTime endDateTime = LocalDateTime.of(2023, 10, 29, 23, 59);

                Random random = new Random();
                long minEpochSeconds = startDateTime.toEpochSecond(ZoneOffset.UTC);
                long maxEpochSeconds = endDateTime.toEpochSecond(ZoneOffset.UTC);
                long randomEpochSeconds = minEpochSeconds + (long)(Math.random() * (maxEpochSeconds - minEpochSeconds + 1));

                LocalDateTime firstDateTime = LocalDateTime.ofEpochSecond(randomEpochSeconds, 0, ZoneOffset.UTC);

                minEpochSeconds = firstDateTime.toEpochSecond(ZoneOffset.UTC);
                randomEpochSeconds = minEpochSeconds + (long)(Math.random() * (maxEpochSeconds - minEpochSeconds + 1));
                LocalDateTime secondDateTime = LocalDateTime.ofEpochSecond(randomEpochSeconds, 0, ZoneOffset.UTC);

                List<LocalDateTime> DateTimes = new ArrayList<>();
                DateTimes.add(firstDateTime);
                DateTimes.add(secondDateTime);

                person.Schedule.put(day, DateTimes);



                LocalTime startTime = LocalTime.of(0,0);
                LocalTime endTime = LocalTime.of(23, 59);

                Random random = new Random();
                int randomHour = random.nextInt(24); // Generate a random hour (0-23)
                int randomMinute = random.nextInt(60); // Generate a random minute (0-59)

                LocalTime firstTime = LocalTime.of(randomHour, randomMinute);
                //LocalTime firstTime = LocalTime.of(20, 30);

                int newRandomHour = random.nextInt(randomHour, 24);

                randomMinute = randomHour != newRandomHour ?
                        random.nextInt(60) : random.nextInt(randomMinute, 60);

                LocalTime secondTime = LocalTime.of(newRandomHour, randomMinute);
                //LocalTime secondTime = LocalTime.of(23, 30);

                List<LocalTime> LocalTimes = new ArrayList<>();
                if(!firstTime.equals(secondTime)) {
                    LocalTimes.add(firstTime);
                    LocalTimes.add(secondTime);
                }
                else {
                    LocalTimes = null;
                }

                person.Schedule.put(day, LocalTimes);
            }*/

            Persons.add(person);
        }
        return Persons;
    }
}
