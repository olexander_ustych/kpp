package Services;

import Models.Person;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class BestTimeFinderService {
    public static AbstractMap.SimpleImmutableEntry<LocalDateTime, LocalDateTime> FindBestTime(List<Person> Persons) {

        LocalDateTime minTime = null;
        LocalDateTime maxTime = null;
        for(var person: Persons)
        {
            for(var sch: person.Schedule)
            {
                //sch.getKey() > minTime
                if(minTime != null)
                {
                    if(sch.getKey().compareTo(maxTime) != 1)
                    {
                        if((sch.getKey().compareTo(minTime)) == 1)
                        {
                            minTime = sch.getKey();
                        }
                    }
                }
                else {
                    minTime = sch.getKey();
                }

                if(maxTime != null)
                {
                    if(sch.getValue().compareTo(maxTime) != -1)
                    {
                        if(sch.getValue().compareTo(maxTime) == -1)
                        {
                            maxTime = sch.getValue();
                        }
                    }
                }
                else {
                    maxTime = sch.getValue();
                }
            }
        }

        return new AbstractMap.SimpleImmutableEntry<>(minTime, maxTime);








        /*TreeMap<DayOfWeek, List<LocalTime>> MeetingDateTime = new TreeMap<>();
        for(var day: DayOfWeek.values()) {
            LocalTime minTime = null;
            LocalTime maxTime = null;

            for(var person: Persons) {
                if(person.Schedule.get(day) != null)
                {
                    if(minTime == null || minTime.isBefore(person.Schedule.get(day).get(0)))
                    {
                        minTime = person.Schedule.get(day).get(0);
                    }

                    if(maxTime == null || maxTime.isAfter(person.Schedule.get(day).get(1)))
                    {
                        maxTime = person.Schedule.get(day).get(1);
                    }
                }
            }

            List<LocalTime> LocalTimes = new ArrayList<>();
            if(minTime.isAfter(maxTime))
            {
                minTime = null;
                maxTime = null;

                /*System.out.println(day.toString());
                System.out.println("\t" + minTime);
                System.out.println("\t" + maxTime);/*

                LocalTimes = null;
            }
            else {

                /*
                DateTimeFormatter _DateTimeFormatter = TimeFormatterService.getDateTimeFormatter();
                System.out.println(day.toString());
                System.out.println("\t" + minTime.format(_DateTimeFormatter));
                System.out.println("\t" + maxTime.format(_DateTimeFormatter));
                System.out.println();/*

                LocalTimes.add(minTime);
                LocalTimes.add(maxTime);
            }

            MeetingDateTime.put(day, LocalTimes);
        }

        return  MeetingDateTime;*/
    }
}
