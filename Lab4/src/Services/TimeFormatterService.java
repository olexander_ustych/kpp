package Services;

import java.time.format.DateTimeFormatter;

public class TimeFormatterService {
    public static DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
    }
    public static DateTimeFormatter getDateTimeEnterPattern() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    }
}
