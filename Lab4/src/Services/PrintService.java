package Services;

import Models.Person;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.List;
import java.util.TreeMap;

public class PrintService {
    public static void PrintPersons(List<Person> persons) {
        for (var person : persons) {
            System.out.println(person.Name + ":");

            for (var datePair : person.Schedule) {

                DateTimeFormatter _DateTimeFormatter = TimeFormatterService.getDateTimeFormatter();
                String FormattedDateTime = datePair.getKey().format(_DateTimeFormatter);
                System.out.println("\t" + FormattedDateTime);

                FormattedDateTime = datePair.getValue().format(_DateTimeFormatter);
                System.out.println("\t" + FormattedDateTime);
                System.out.println();
            }





            /*for(var day: DayOfWeek.values()) {
                System.out.println("\t" + day.toString());
                if(person.Schedule.get(day) != null)
                {
                    for(var date: person.Schedule.get(day)) {
                        //DateTimeFormatter _DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                        DateTimeFormatter _DateTimeFormatter = TimeFormatterService.getDateTimeFormatter();
                        String FormattedDateTime = date.format(_DateTimeFormatter);
                        System.out.println("\t\t" + FormattedDateTime);
                    }
                }
                else {
                    System.out.println("\t\t" + null);
                    System.out.println("\t\t" + null);
                }
            }
            System.out.println();*/
        }
    }

    //TreeMap<DayOfWeek, List<LocalTime>> Meetings
    public static void PrintDaysMeeting(AbstractMap.SimpleImmutableEntry<LocalDateTime, LocalDateTime> bestTime) {

        System.out.println("Best time: ");
        DateTimeFormatter _DateTimeFormatter = TimeFormatterService.getDateTimeFormatter();
        String FormattedDateTime = bestTime.getKey().format(_DateTimeFormatter);
        System.out.println("\t" + FormattedDateTime);

        FormattedDateTime = bestTime.getValue().format(_DateTimeFormatter);
        System.out.println("\t" + FormattedDateTime);






        /*for(var day: Meetings.keySet()) {
            System.out.println(day.toString());

            LocalTime minTime = null;
            LocalTime maxTime = null;
            if(Meetings.get(day) != null)
            {
                minTime = Meetings.get(day).get(0);
                maxTime = Meetings.get(day).get(1);

                DateTimeFormatter _DateTimeFormatter = TimeFormatterService.getDateTimeFormatter();
                System.out.println("\t" + minTime.format(_DateTimeFormatter));
                System.out.println("\t" + maxTime.format(_DateTimeFormatter));
            }
            else {
                System.out.println("\t" + minTime);
                System.out.println("\t" + maxTime);
            }
            System.out.println();
        }*/
    }

    public static void PrintDuration(Duration duration) {
        System.out.println("\tDays: " + duration.toDays());
        System.out.println("\tHours: " + duration.toHours() % 24);
        System.out.println("\tMinutes: " + duration.toMinutes() % 60);
    }
}