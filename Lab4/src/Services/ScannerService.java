package Services;

import Models.Person;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScannerService {
    public static List<Person> EnterData() {
        List<Person> persons = new ArrayList<>();
        Integer choose = null;

        Scanner newSc = new Scanner(System.in);
        do {
            System.out.println("0 - out, 1 - next");
            choose = newSc.nextInt();
            Person person = new Person();
            switch (choose)
            {
                case 0: {
                    break;
                }
                case 1: {
                    System.out.println("Enter name: ");
                    person.Name = newSc.next();

                    System.out.println("Enter count of periods: ");
                    int count = newSc.nextInt();

                    person.Schedule = new ArrayList<>();
                    for (int i = 0; i < count ; i++)
                    {
                        newSc.nextLine();
                        System.out.println("Enter first date (template: 'yyyy-MM-dd HH:mm'): ");
                        String firstDateString = newSc.nextLine();
                        System.out.println("Enter second date (template: 'yyyy-MM-dd HH:mm'): ");
                        String secondDateString = newSc.nextLine();

                        var patternFormat = TimeFormatterService.getDateTimeEnterPattern();
                        LocalDateTime firstDate = LocalDateTime.parse(firstDateString, patternFormat);
                        LocalDateTime secondDate = LocalDateTime.parse(secondDateString, patternFormat);

                        person.Schedule.add(new AbstractMap.SimpleImmutableEntry<>(firstDate, secondDate));
                    }

                    break;
                }
                default: {
                    break;
                }
            }
            if(person.Name != null)
            {
                persons.add(person);
            }
        } while(choose != 0);
        newSc.close();

        return persons;
    }
}
