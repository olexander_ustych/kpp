import Classes.SequenceCalculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter count: ");
        Scanner newSc = new Scanner(System.in);
        int n = newSc.nextInt();

        /*double res = 1;
        for (int i = 0; i < n; i++)
        {
            res += (1.0 / (double)(i) );
        }

        System.out.println("Res: " + res);*/

        SequenceCalculator calculator = new SequenceCalculator(n);
        var res = calculator.Calculate();
        System.out.println(res);
    }
}