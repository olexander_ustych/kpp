package Classes;

import java.math.BigInteger;

public class Gcd {
    public static Integer findGcdInteger(Integer a, Integer b)
    {
        if (a == 0)
            return b;

        return findGcdInteger(b % a, a);
    }
    public static BigInteger findGcdBigInteger(BigInteger a, BigInteger b)
    {
        if (a.compareTo(BigInteger.ZERO) == 0)
            return b;

        return findGcdBigInteger(b.mod(a), a);

    }
}
