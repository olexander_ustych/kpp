package Classes;

import java.math.BigInteger;
import java.text.MessageFormat;

public class SequenceCalculator {
    private final int length;
    private final Fraction<Integer> fraction;

    public  SequenceCalculator(int Length) {
        this.length = Length;
        this.fraction = new Fraction<>(0, 1);
    }

    public String Calculate() {
        var numerator = this.fraction.GetNumerator();
        var denominator = this.fraction.GetDenominator();

        for (int i = 1; i <= this.length; i++) {
            denominator *= i;
            if(denominator < 0)
            {
                return CalculateBigger();
            }
        }
        for (int i = 1; i <= this.length; i++) {
            numerator += denominator / i;
            if(numerator < 0)
            {
                return CalculateBigger();
            }
        }

        var gcd = Gcd.findGcdInteger(numerator, denominator);
        this.fraction.SetNumerator(numerator / gcd);
        this.fraction.SetDenominator(denominator / gcd);

        return MessageFormat.format("{0} = {1}", this.fraction.toString(),
                (double)this.fraction.GetNumerator() / (double)this.fraction.GetDenominator());
    }

    private String CalculateBigger() {
        Fraction<BigInteger> BigFraction = new Fraction<>(
                new BigInteger(this.fraction.GetNumerator().toString()),
                new BigInteger(this.fraction.GetDenominator().toString())
        );

        var numerator = BigFraction.GetNumerator();
        var denominator = BigFraction.GetDenominator();

        for (int i = 1; i <= this.length; i++) {
            denominator = denominator.multiply(BigInteger.valueOf(i));
        }
        for (int i = 1; i <= this.length; i++) {
            numerator = numerator.add(denominator.divide(BigInteger.valueOf(i)));
        }

        var gcd = Gcd.findGcdBigInteger(numerator, denominator);
        BigFraction.SetNumerator(numerator.divide(gcd));
        BigFraction.SetDenominator(denominator.divide(gcd));

        double dNumerator = BigFraction.GetNumerator().doubleValue();
        double dDenominator = BigFraction.GetDenominator().doubleValue();
        return MessageFormat.format("{0} = {1}", BigFraction.toString(),
                dNumerator/dDenominator);
    }
}