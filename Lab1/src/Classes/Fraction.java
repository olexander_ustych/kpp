package Classes;

import java.text.MessageFormat;

public class Fraction<T> {
    private T numerator, denominator;

    public Fraction() { }
    public Fraction(T numerator, T denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public void SetNumerator(T NewNumerator) {
        this.numerator = NewNumerator;
    }
    public T GetNumerator() {
        return this.numerator;
    }
    public void SetDenominator(T NewDenominator) {
        this.denominator = NewDenominator;
    }
    public T GetDenominator() {
        return  this.denominator;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0}/{1}", this.numerator, this.denominator);
    }
}
