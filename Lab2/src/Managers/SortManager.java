package Managers;

import Christmas_decorations.Attributes.Interfaces.IDecoration;
import java.util.Comparator;

public class SortManager {
    public static class NameSortingClass implements Comparator<IDecoration> {
        @Override
        public int compare(IDecoration d1, IDecoration d2) {
            return d1.GetName().compareTo(d2.GetName());
        }
    }

    public class ColorSortingClass implements Comparator<IDecoration> {
        @Override
        public int compare(IDecoration c1, IDecoration c2) {
            return c1.GetColor().compareTo(c2.GetColor());
        }
    }

    /*public void NameSorting(List<IDecoration> collection) {
        collection.sort(new NameSortingClass());
    }

    public void ColorSorting(List<IDecoration> collection) {
        collection.sort(new ColorSortingClass());
    }

    public void StyleSorting(List<IDecoration> collection) {
        Collections.sort(collection, new Comparator<IDecoration>() {
            @Override
            public int compare(IDecoration d1, IDecoration d2) {
                return d1.GetStyle().compareTo(d2.GetStyle());
            }
        });
    }

    public void NameSortingByLambda(List<IDecoration> collection) {
        Collections.sort(collection, (d1, d2) -> d1.GetName().compareTo(d2.GetName()));
    }*/
}
