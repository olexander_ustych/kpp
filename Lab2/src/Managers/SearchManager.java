package Managers;

import Christmas_decorations.Attributes.Color;
import Christmas_decorations.Attributes.Interfaces.IDecoration;
import Christmas_decorations.Attributes.Style;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SearchManager {
    public static <T extends IDecoration> List<T> Search(List<IDecoration> collection,
                                                                          Class<T> ClassType) {
        var result = new ArrayList<T>();
        for(IDecoration decoration : collection){
            if(ClassType.isInstance(decoration)) {
                result.add((T)decoration);
            }
        }
        return result;
    }

    public static List<IDecoration> Search(List<IDecoration> collection, Color color) {
        var result = new ArrayList<IDecoration>();
        for (IDecoration decoration : collection) {
            if(decoration.GetColor() == color) {
                result.add(decoration);
            }
        }
        return result;
    }

    public static List<IDecoration> Search(List<IDecoration> collection, Style style) {
        var result = new ArrayList<IDecoration>();
        for (IDecoration decoration : collection) {
            if(decoration.GetStyle() == style) {
                result.add(decoration);
            }
        }
        return result;
    }
}
