import Christmas_decorations.Attributes.Color;
import Christmas_decorations.Attributes.Implementations.ChristmasTreeDecoration;
import Christmas_decorations.Attributes.Implementations.HouseDecoration;
import Christmas_decorations.Attributes.Implementations.StreetDecoration;
import Christmas_decorations.Attributes.Interfaces.IDecoration;
import Christmas_decorations.Attributes.Style;
import Managers.SearchManager;
import Managers.SortManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<IDecoration> collection = new ArrayList<>();
        collection.add(new ChristmasTreeDecoration("ch_tree_decor", Color.Blue, Style.Toy));
        collection.add(new ChristmasTreeDecoration("ch_tree_red_ball", Color.Red, Style.Ball));
        collection.add(new ChristmasTreeDecoration("ch_tree_snow", Color.White, Style.Snowflake));
        collection.add(new ChristmasTreeDecoration("ch_tree_pt", Color.Pink, Style.Toy));
        collection.add(new HouseDecoration("house_d_bl-b", Color.Blue, Style.Ball));
        collection.add(new HouseDecoration("house_d_yf", Color.Yellow, Style.Figure));
        collection.add(new HouseDecoration("house_d_b_snow", Color.Blue, Style.Snowflake));
        collection.add(new HouseDecoration("house_d_gb", Color.Green, Style.Ball));
        collection.add(new HouseDecoration("house_d_red-f", Color.Red, Style.Figure));
        collection.add(new StreetDecoration("street_dec_gb", Color.Green, Style.Ball));
        collection.add(new StreetDecoration("street_dec_rt", Color.Red, Style.Toy));
        collection.add(new StreetDecoration("street_dec_yf", Color.Yellow, Style.Figure));
        collection.add(new StreetDecoration("street_dec_gs", Color.Green, Style.Snowflake));
        collection.add(new StreetDecoration("street_dec_bb", Color.Blue, Style.Ball));

        System.out.println(collection.get(2));



        var HouseDecorations = SearchManager.Search(collection, HouseDecoration.class);
        System.out.println("House decoration: ");
        for(IDecoration element : HouseDecorations) {
            element.decorate();
        }

        var BlueDecoration = SearchManager.Search(collection, Color.Blue);
        System.out.println("\nBlue decoration: ");
        for(IDecoration element : BlueDecoration) {
            element.decorate();
        }

        var BallDecoration = SearchManager.Search(collection, Style.Ball);
        System.out.println("\nBall decoration: ");
        for(IDecoration element : BallDecoration) {
            element.decorate();
        }

        System.out.println("\n\n");

        SortManager SortingManager = new SortManager();
        var HouseDecorationsCopy = new ArrayList<IDecoration>(HouseDecorations);
        System.out.println("Houses Copy: ");
        for (IDecoration element : HouseDecorationsCopy) {
            System.out.println(element.toString());
        }
        System.out.println();

        HouseDecorations.sort(new SortManager.NameSortingClass());
        System.out.println("Houses sorted by name: ");
        for (IDecoration element : HouseDecorations) {
            System.out.println(element.toString());
        }
        System.out.println();

        BallDecoration.sort(SortingManager.new ColorSortingClass());
        System.out.println("Blue sorted by style: ");
        for (IDecoration element : BallDecoration) {
            System.out.println(element.toString());
        }
        System.out.println();

        BlueDecoration.sort(new Comparator<IDecoration>() {
            @Override
            public int compare(IDecoration d1, IDecoration d2) {
                return d1.GetStyle().compareTo(d2.GetStyle());
            }
        });
        System.out.println("Ball sorted by color: ");
        for (IDecoration element : BlueDecoration) {
            System.out.println(element.toString());
        }
        System.out.println();

        HouseDecorationsCopy.sort((d1, d2) -> d1.GetName().compareTo(d2.GetName()));
        System.out.println("HouseCopy sorted by name: ");
        for (IDecoration element : HouseDecorationsCopy) {
            System.out.println(element.toString());
        }
        System.out.println();

        HouseDecorations.sort(new SortManager.NameSortingClass().reversed());
        System.out.println("Houses sorted by name order by descending: ");
        for (IDecoration element : HouseDecorations) {
            System.out.println(element.toString());
        }
        System.out.println();
    }
}