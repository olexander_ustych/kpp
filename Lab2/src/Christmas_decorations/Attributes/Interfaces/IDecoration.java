package Christmas_decorations.Attributes.Interfaces;

import Christmas_decorations.Attributes.Color;
import Christmas_decorations.Attributes.Style;

public interface IDecoration {
    void SetName(String name);
    String GetName();
    void SetColor(Color color);
    Color GetColor();
    void SetStyle(Style style);
    Style GetStyle();
    void decorate();
}
