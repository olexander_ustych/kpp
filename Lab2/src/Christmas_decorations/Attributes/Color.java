package Christmas_decorations.Attributes;

public enum Color {
    Red,
    Green,
    Blue,
    Yellow,
    White,
    Pink
}
