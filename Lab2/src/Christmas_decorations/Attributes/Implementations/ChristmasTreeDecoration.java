package Christmas_decorations.Attributes.Implementations;

import Christmas_decorations.Attributes.Color;
import Christmas_decorations.Attributes.Interfaces.IDecoration;
import Christmas_decorations.Attributes.Style;

public class ChristmasTreeDecoration implements IDecoration {
    private String name;
    private Color color;
    private Style style;

    public ChristmasTreeDecoration() {}
    public ChristmasTreeDecoration(String name, Color color, Style style) {
        this.name = name;
        this.color = color;
        this.style = style;
    }

    @Override
    public void SetName(String name) {
        this.name = name;
    }

    @Override
    public String GetName() {
        return this.name;
    }

    @Override
    public void SetColor(Color color) {
        this.color = color;
    }

    @Override
    public Color GetColor() {
        return this.color;
    }

    @Override
    public void SetStyle(Style style) {
        this.style = style;
    }

    @Override
    public Style GetStyle() {
        return this.style;
    }

    @Override
    public void decorate() {
        System.out.println("Decorating the Christmas tree with "
                + name + " which is " + color.toString() + " " + style.toString() + " ornaments.");
    }

    @Override
    public String toString() {
        return "ChristmasTreeDecoration: " + name + " " +
                color.toString() + " " + style.toString();
    }
}
