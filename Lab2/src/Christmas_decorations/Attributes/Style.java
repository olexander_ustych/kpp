package Christmas_decorations.Attributes;

public enum Style {
    Toy,
    Figure,
    Ball,
    Snowflake
}
