package Models;

import java.time.LocalDate;

public class Bookkeeping {
    public TicketSubscriber Taker;
    public Book Book;
    public LocalDate TakingDate;
    public LocalDate ReturnDate;
    public LocalDate RealReturnDate;
}
