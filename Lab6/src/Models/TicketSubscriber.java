package Models;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Flow;

public class TicketSubscriber implements Serializable {
    public String SubscriberFirstName;
    public String SubscriberLastName;
    public String SubscriberFatherName;
    public String Email;
    public List<Book> Books = new LinkedList<>();

    public TicketSubscriber(String subscriberFirstName, String subscriberLastName, String subscriberFatherName,
                            String email)
    {
        this.SubscriberFirstName = subscriberFirstName;
        this.SubscriberLastName = subscriberLastName;
        this.SubscriberFatherName = subscriberFatherName;
        this.Email = email;
    }

    public static int comapareBookCount(TicketSubscriber r1, TicketSubscriber r2) {
        if(r1.Books.size() < r2.Books.size()) {
            return -1;
        } else if (r1.Books.size() == r2.Books.size()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        var Another = ((TicketSubscriber) obj);
        if(this.SubscriberFirstName.equals(Another.SubscriberFirstName) &&
            this.SubscriberLastName.equals(Another.SubscriberLastName) &&
            this.SubscriberFatherName.equals(Another.SubscriberFatherName) &&
            this.Email.equals(Another.Email))
        {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Reader: " + SubscriberFirstName + " " + SubscriberLastName + " " + SubscriberFatherName +
            ", Email: " + Email;
    }
}
