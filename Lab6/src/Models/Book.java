package Models;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Book implements Serializable {
    public String Author;
    public String Title;
    public int PublicationYear;

    public Book(String author, String title, int publicationYear) {
        this.Author = author;
        this.Title = title;
        this.PublicationYear = publicationYear;
    }

    public static int compareYear(Book b1, Book b2) {
        if(b1.PublicationYear < b2.PublicationYear) {
            return -1;
        } else if(b1.PublicationYear == b2.PublicationYear) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        var AnotherBook = ((Book) obj);
        if(this.Title.equals(AnotherBook.Title) && this.Author.equals(AnotherBook.Author) &&
            this.PublicationYear == AnotherBook.PublicationYear)
        {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Author: " + Author + ", Title: " + Title + ", PublicationYear: " + PublicationYear;
    }
}
