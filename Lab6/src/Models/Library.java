package Models;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Library {
    public List<Book> AllBooks = new LinkedList<>();
    public List<TicketSubscriber> AllSubscribers = new LinkedList<>();
    public List<Bookkeeping> AllBookkeeping = new LinkedList<>();

    public Library(List<TicketSubscriber> readers, List<Book> books) {
        AllBooks = books;
        AllSubscribers = readers;
    }

    public void TakeBook(TicketSubscriber taker, Book book, LocalDate takingDate, int howManyDaysShouldTake) {
        taker.Books.add(book);
        var bookkeeping = new Bookkeeping();
        bookkeeping.Taker = taker;
        bookkeeping.Book = book;
        bookkeeping.TakingDate = takingDate;//LocalDate.now();
        bookkeeping.ReturnDate = bookkeeping.TakingDate.plusDays(howManyDaysShouldTake);
        AllBookkeeping.add(bookkeeping);
    }

    public void ReturnBook(TicketSubscriber taker, Book book, LocalDate realReturnDate) {
        taker.Books.remove(book);
        var bookkeeping = AllBookkeeping.stream().filter(b -> {
          if(b.Book.equals(book) && b.Taker.equals(taker))
          {
              return true;
          }
          return false;
        }).findFirst().get();
        bookkeeping.RealReturnDate = realReturnDate;
    }

    public List<TicketSubscriber> CreateListOfDebtors(LocalDate date) {
        return AllBookkeeping.stream().filter(k -> {
            if(k.RealReturnDate == null) {
                if(k.ReturnDate.compareTo(date) < 0) {
                    return true;
                }
            }
            return false;
        }).map(k -> k.Taker).collect(Collectors.toList());
    }
}
