import Models.Book;
import Models.Library;
import Models.TicketSubscriber;
import Services.DataService;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        DataService.WriteInFileBooksAndReaders();
        var books = DataService.DeserializeBooks();
        var readers = DataService.DeserializeReaders();

        while(true) {
            System.out.print("\n\n\n");
            System.out.println("1 - Sort books by year\n" +
                    "2 - addresses of readers who have more that 2 books\n" +
                    "3 - check how many readers take books of specific author\n" +
                    "4 - find who take the most books\n" +
                    "5 - find who have less than 2 books, find who have more that 2 books\n" +
                    "6 - find debtors\n" +
                    "0 - exit");

            List<Integer> choiceValue = Arrays.asList(0, 1, 2, 3, 4, 5, 6);

            Integer choice = null;
            do {
                System.out.print("Enter your choice: ");
                Scanner sc = new Scanner(System.in);
                try {
                    choice = sc.nextInt();
                } catch (Exception e) {
                    System.out.println("Invalid data entered");
                    choice = null;
                }
            } while (!choiceValue.contains(choice));

            switch (choice) {
                case 0: {
                    //0 - exit
                    books.stream().map(b -> b.PublicationYear + 10);
                    break;
                }
                case 1: {
                    //1 - Sort books by year
                    System.out.println();
                    System.out.println("Books sorted by year:");
                    books.stream().sorted(Book::compareYear).
                            forEach(x -> System.out.println(x.toString()));
                    break;
                }
                case 2: {
                    //2 - addresses of readers who have more that 2 books
                    readers.get(0).Books = Arrays.asList(books.get(1), books.get(5), books.get(7));
                    readers.get(2).Books = Arrays.asList(books.get(2), books.get(7), books.get(9));

                    readers.stream().filter(r -> r.Books.size() > 2).forEach(x -> System.out.println(x.toString()));

                    readers.get(0).Books = new LinkedList<>();
                    readers.get(2).Books = new LinkedList<>();
                    break;
                }
                case 3: {
                    //3 - check how many readers take books of specific author
                    readers.get(0).Books = Arrays.asList(books.get(3));
                    readers.get(1).Books = Arrays.asList(books.get(3));
                    //William Shakespeare

                    Scanner sc = new Scanner(System.in);
                    try {
                        System.out.print("Enter author name: ");
                        final String authorName = sc.nextLine();
                        var count = readers.stream().filter(r ->
                                        r.Books.stream().anyMatch(b -> b.Author.equals(authorName))).
                                count();
                        System.out.println("Count: " + count);
                        readers.stream().filter(r ->
                                        r.Books.stream().anyMatch(b -> b.Author.equals(authorName))).
                                forEach(x -> System.out.println(x.toString()));
                    } catch (Exception e) {
                        System.out.println("Invalid data entered");
                    }

                    readers.get(0).Books = new LinkedList<>();
                    readers.get(1).Books = new LinkedList<>();

                    break;
                }
                case 4: {
                    //4 - find who take the most books
                    readers.get(0).Books = Arrays.asList(books.get(0));
                    readers.get(1).Books = Arrays.asList(books.get(1), books.get(3));
                    readers.get(2).Books = Arrays.asList(books.get(2), books.get(4), books.get(5));

                    var readerWithMostBooks = readers.stream().
                            max(TicketSubscriber::comapareBookCount).get();
                    System.out.println("Books count: " + readerWithMostBooks.Books.size());
                    System.out.println("Reader, who takes these books: " + readerWithMostBooks.toString());
                    System.out.println("Books: ");
                    readerWithMostBooks.Books.stream().forEach(x -> System.out.println("\t" + x.toString()));

                    readers.get(0).Books = new LinkedList<>();
                    readers.get(1).Books = new LinkedList<>();
                    readers.get(2).Books = new LinkedList<>();

                    break;
                }
                case 5: {
                    //5 - find who have less than 2 books, find who have more that 2 books
                    readers.get(0).Books = Arrays.asList(books.get(0));
                    readers.get(1).Books = Arrays.asList(books.get(1), books.get(3), books.get(6));
                    readers.get(2).Books = Arrays.asList(books.get(2), books.get(4), books.get(5));

                    var readersWithLessThan2Books = readers.stream().filter(r -> r.Books.size() < 2).
                            collect(Collectors.toList());
                    System.out.println("Readers with less than 2 books: ");
                    readersWithLessThan2Books.stream().forEach(x -> System.out.println("\t" + x.toString()));
                    var readersWithMoreThan1Books = readers.stream().filter(r -> r.Books.size() >= 2).
                            collect(Collectors.toList());
                    System.out.println("Readers with more than 1 books: ");
                    readersWithMoreThan1Books.stream().forEach(x -> System.out.println("\t" + x.toString()));

                    readers.get(0).Books = new LinkedList<>();
                    readers.get(1).Books = new LinkedList<>();
                    readers.get(2).Books = new LinkedList<>();

                    break;
                }
                case 6: {
                    //find debtors
                    var library = new Library(readers, books);
                    library.TakeBook(readers.get(0), books.get(0),
                            LocalDate.of(2023, 1, 15), 14);
                    // Return date: 2023-1-29
                    library.TakeBook(readers.get(1), books.get(1),
                            LocalDate.of(2023, 1, 15), 5);
                    library.TakeBook(readers.get(2), books.get(2),
                            LocalDate.of(2023, 1, 15), 7);
                    // Return date: 2023-1-22

                    library.ReturnBook(readers.get(1), books.get(1), LocalDate.of(2023, 1, 19));

                    var debtors = library.CreateListOfDebtors(
                            LocalDate.of(2023, 1, 25));

                    System.out.println("Debtors: ");
                    debtors.stream().forEach(d -> System.out.println("\t" + d.toString()));

                    break;
                }
                default: {
                    break;
                }
            }
            if(choice == 0)
            {
                break;
            }
        }
    }
}