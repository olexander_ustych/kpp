package Services;

import Models.Book;
import Models.TicketSubscriber;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataService {
    final static String BooksFile = "C:\\Users\\FireM\\Java IDEA\\KPP_Java\\Lab6\\src\\SourceFiles\\Books.txt";
    final static String ReadersFile = "C:\\Users\\FireM\\Java IDEA\\KPP_Java\\Lab6\\src\\SourceFiles\\TicketSubscribers.txt";

    private static List<Book> CreateBooks() {
        var Books = new ArrayList<Book>();

        Books.add(new Book("F. Scott Fitzgerald", "The Great Gatsby", 1925));
        Books.add(new Book("Paulo Coelho", "The Alchemist", 1988));
        Books.add(new Book("J. K. Rowling", "Harry Potter", 1997));
        Books.add(new Book("William Shakespeare", "Romeo and Juliet", 1597));
        Books.add(new Book("Gabriel García Márquez", "One Hundred Years of Solitude", 1967));
        Books.add(new Book("George OrWell", "1984", 1948));
        Books.add(new Book("J. D. Salinger", "The Catcher in the Rye", 1951));
        Books.add(new Book("Oscar Wilde", "The Picture of Dorian Gray", 1890));
        Books.add(new Book("Daniel Keyes", "Flowers for Algernon", 1959));
        Books.add(new Book("Miguel de Cervantes", "Don Quixote", 1605));

        return Books;
    }

    private static List<TicketSubscriber> CreateTicketSubscriber() {
        var TicketSubscribers = new ArrayList<TicketSubscriber>();

        TicketSubscribers.add(new TicketSubscriber("Vasyl", "Peleshko",
                "Petrovych", "vasyl@email"));
        TicketSubscribers.add(new TicketSubscriber("Olexander", "Ustych",
                "Yuriyovuch", "olexander@email"));
        TicketSubscribers.add(new TicketSubscriber("Taras", "Marynyak",
                "Myhailovych", "taras@email"));

        return TicketSubscribers;
    }

    public static void WriteInFileBooksAndReaders() {
        var readers = DataService.CreateTicketSubscriber();
        var books = DataService.CreateBooks();

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(BooksFile))) {
            oos.writeObject(books);
            //System.out.println("Object serialized successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ReadersFile))) {
            oos.writeObject(readers);
            //System.out.println("Object serialized successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Book> DeserializeBooks() {
        List<Book> books = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(BooksFile))) {
            books = (List<Book>) ois.readObject();
            //System.out.println("Object deserialized successfully.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<TicketSubscriber> DeserializeReaders() {
        List<TicketSubscriber> books = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ReadersFile))) {
            books = (List<TicketSubscriber>) ois.readObject();
            //System.out.println("Object deserialized successfully.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return books;
    }
}
